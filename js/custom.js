$(function () {
  'use strict';
  //counter jquery
  $('.counter').countUp({
    delay: 10,
    time: 1000,
  });
  //
  $('body').css('paddingTop', $('.navbar').innerHeight());
  //
  $('.navbar li a.click-link, .footer .contact a.links').click(function (e) {
    e.preventDefault();
    $('html, body').animate(
      {
        scrollTop: $('#' + $(this).data('scroll')).offset().top + 1,
      },
      1000
    );
  });
  //
  $('.navbar ul li a').click(function () {
    $(this)
      .addClass('active')
      .parent()
      .siblings()
      .find('a')
      .removeClass('active');
  });
  //loading screen
  $(window).load(function () {
    $('.loading').fadeOut(1000);
  });
  // create wow
  new WOW().init();
});

const res = fetch('https://jsonplaceholder.typicode.com/photos')
  .then((response) => response.json())
  .then((json) => {
    json.forEach((el) => {
      if (el.id <= 4) {
        $('#products .row').append(`
            <div class="col-lg-3 col-sm-6 num">
            <div class="product scrol">
                <div class="image">
                    <img src="${el.url}">
                </div>
                <div class="del">
                    <h5 class="wow fadeInUp" data-wow-delay="0.0s">${el.title}</h5>
                   
                    <button class="wow fadeInUp" data-wow-delay="0.4s"><a href="#">Contact us</a></button>
                </div>
            </div>
        </div>`);
      }

      console.log(el);
    });
  });
